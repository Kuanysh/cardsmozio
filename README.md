# CardsMozio



## Tech Stack

Kotlin and Junit5 are used to solve and test this task.

## Assumption

I am going to assume there is a reasonable amount of cards per player, not millions.
In face to face interview I would ask clarifying questions.


## Solution

Step 1:
When each player, Jane and Alice, receive cards I count how many cards they have of each kind using HashMap.
HashMap allows storing card and occurrence as key value pair. 

Putting and retrieving takes constant time. Space is linear.

Step 2:
Compare each player's hand vs the other.
When a player has 1 or less of each card kind while the other has more than 2 I add to a list of cards the player will receive. 
Adding to a list is constant and converting to Array is linear time in Kotlin.

Traversing through key-value entries of each hashmap and adding to list is linear time. Space is linear.

Step 3:
Return array of cards Jane and Alice will receive.

## Conclusion

The time and space complexity it takes to solve this task are linear.
