class Cards(val jane: IntArray, val alice: IntArray) {
    fun exchange(): Pair<IntArray, IntArray> {
        val janeCardsMap = toCardOccurrenceMap(jane)
        val aliceCardsMap = toCardOccurrenceMap(alice)

        val janeReceives = toReceive(janeCardsMap, aliceCardsMap)
        val aliceReceives = toReceive(aliceCardsMap, janeCardsMap)

        return janeReceives to aliceReceives
    }

    private fun toCardOccurrenceMap(cards: IntArray): Map<Int, Int> {
        val map = HashMap<Int, Int>()
        cards.forEach { card ->
            val occurrence = map.getOrElse(card) { 0 }
            map[card] = occurrence + 1
        }
        return map
    }

    private fun toReceive(own: Map<Int, Int>, other: Map<Int, Int>): IntArray {
        val toReceive = mutableListOf<Int>()
        other.forEach { (otherCard, otherOccurrence) ->
            val ownOccurrence = own[otherCard] ?: 0
            if (ownOccurrence <= 1 && otherOccurrence > 2) {
                repeat(otherOccurrence - 2) {
                    toReceive.add(otherCard)
                }
            }
        }
        return toReceive.toIntArray()
    }
}
