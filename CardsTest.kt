import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class CardsByMapTest {

    @ParameterizedTest
    @MethodSource("cardsGameProvider")
    fun `GIVEN jane and alice cards WHEN exchange THEN return what jane and alice receive`(
        janeHand: IntArray,
        aliceHand: IntArray,
        janeExpectedToReceive: IntArray,
        aliceExpectedToReceive: IntArray
    ) {
        // GIVEN
        val game = Cards(janeHand, aliceHand)

        // WHEN
        val (janeReceives, aliceReceives) = game.exchange()

        // THEN
        assertArrayEquals(janeExpectedToReceive, janeReceives)
        assertArrayEquals(aliceExpectedToReceive, aliceReceives)
    }

    companion object {
        @JvmStatic
        fun cardsGameProvider() = arrayOf(
            Arguments {
                val jane = intArrayOf(1, 7, 3, 1, 7, 4, 5, 1, 7, 1)
                val alice = intArrayOf(2, 3, 3, 2, 4, 3, 2)
                val janeExpected = intArrayOf(2, 3)
                val aliceExpected = intArrayOf(1, 1, 7)
                arrayOf(jane, alice, janeExpected, aliceExpected)
            },
            Arguments {
                val jane = intArrayOf(1, 2, 3, 4, 4)
                val alice = intArrayOf(4, 4, 4, 5, 6, 7)
                val janeExpected = intArrayOf()
                val aliceExpected = intArrayOf()
                arrayOf(jane, alice, janeExpected, aliceExpected)
            },
            Arguments {
                val jane = intArrayOf(5, 4, 4, 3, 3, 3, 3)
                val alice = intArrayOf(1, 3)
                val janeExpected = intArrayOf()
                val aliceExpected = intArrayOf(3, 3)
                arrayOf(jane, alice, janeExpected, aliceExpected)
            }
        )
    }
}
